from nose import with_setup
from nose.tools import raises

import calc

test_list = []


def setup_func():
    global test_list
    for i in range(10):
        test_list.append(i)


@with_setup(setup_func)
def test_make_sum():
    assert calc.make_sum([3, 4, 7]) == 14
    assert calc.make_sum([]) == 0
    assert calc.make_sum([3, -10, 2]) == -5
    assert calc.make_sum([3]) == 3
    assert calc.make_sum(test_list) == 45


@raises(TypeError)
def test_make_sum_bad_list():
    calc.make_sum([3, 4, 'seven'])


@raises(ValueError)
def test_make_sum_bad_map():
    calc.make_sum(map(int, [3, 4, 'seven']))
