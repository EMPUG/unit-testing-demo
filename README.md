# Python Unit Testing with Nose

`calc.py` and `test_calc.py` were used in a demonstration of Python unit testing
with Nose, given to the Eastern Michigan Python User Group (EMPUG) on 2020-08-15.

During the meeting we wrote the files from scratch. After the meeting I made
some enhancements based on the discussion at the end of the meeting.
 
## Installation

1. Navigate to a directory somewhere where you keep your software projects:

        cd projects

2. Clone the unit-testing-demo repository:

        git clone https://gitlab.com/EMPUG/unit-testing-demo.git
        
3. Navigate to the new `unit-testing-demo` directory which contains the repository.

        cd unit-testing-demo

4. Create a Python 3 virtual environment called `env`:

        python3 -m venv env

5. Activate the environment:

        source env/bin/activate

    (Note: the above command is different under Windows.)

6. Install required packages (nose and coverage):

        pip install -r requirements.txt

## Usage

`calc.py` will sum or multiply a list of integers and print the result on the console.
For example:

        python calc.py sum 3 4 12
        
        python calc.py mult 3 4 -5
        
`nosetests` runs the test functions found in `test_calc.py`:

        nosetests

You can get test coverage by including the `--with-coverage` option. (This works
because the coverage package is installed.)

        nosetests --with-coverage
