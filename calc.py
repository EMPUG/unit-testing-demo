import sys
from functools import reduce


def make_sum(numbers):
    return sum(numbers)


def make_product(numbers):
    return reduce(lambda a, b: a * b, numbers)


def main():
    calc_functions = {
        'sum': make_sum,
        'mult': make_product,
    }

    operator = sys.argv[1]
    operands = map(int, sys.argv[2:])

    if operator not in calc_functions:
        print('Error: invalid operator')
        return

    try:
        result = calc_functions[operator](operands)
    except (ValueError, TypeError):
        print('Error: operands must be integers')
        return

    print(result)


if __name__ == '__main__':
    main()
